#include <iostream> // print to console (cout)
//#include <cmath> // math operations
//#include <fstream> // file streaming for input file
#include "Model.hpp"


int main(int argc, char *argv[])
{

  std::string fname;
  std::string odir;
  if(argc<3) {
    std::cerr << "Not enough arguments:" 
	      << " ./basic_example <sim-name> <output-directory>" << std::endl;
    return 0;
  }
  else {
    fname = argv[1];
    odir = argv[2];
  }
    
  
  Model my_model;

  std::cout << "read input" << std::endl;
  my_model.read_input(fname+".inp");

  my_model.assemble(); // Get unconstrained stiffness matrix + load vector
  my_model.apply_BC();
  std::cout << "solve" << std::endl;
  my_model.solve(); // Solve linear system and get displacement vector
  my_model.compute_reaction(); // Calculate reaction
  my_model.print_results(odir,fname); // print results
  


  return 0;
}
